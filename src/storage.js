import StorageEvent from './event';
import Merge from 'deepmerge';

const $fnExtractKey = Symbol('fnExtractKey');
const $fnResolveSet = Symbol('fnResolveSet');
const $fnResolveGet = Symbol('fnResolveGet');

/**
 * Storage Bridge
 */
export default class {
    /**
     * @param {Object} storage
     */
    constructor(storage) {
        this.storage = storage;
        this.options = {
            namespace: '',
            events: ['storage'],
        };

        Object.defineProperty(this, 'length', {
            /**
             * Define length property
             *
             * @return {number}
             */
            get() {
                return this.storage.length;
            },
        });

        if (typeof window !== 'undefined') {
            for (const i in this.options.events) {
                if (window.addEventListener) {
                    window.addEventListener(this.options.events[i], StorageEvent.emit, false);
                } else if (window.attachEvent) {
                    window.attachEvent(`on${this.options.events[i]}`, StorageEvent.emit);
                } else {
                    window[`on${this.options.events[i]}`] = StorageEvent.emit;
                }
            }
        }
    }

    /**
     * Extract key and path from field name
     *
     * @param key
     * @returns {{key, path}}
     */
    [$fnExtractKey](key){
        key = key.split('.');

        return {
            key: key[0],
            path: key.slice(1)
        }
    }

    /**
     * @param extracted
     * @param value
     * @returns {*}
     *
     * @todo: REFACTORING!!!
     */
    [$fnResolveSet](extracted, value){
        let current = this.get(extracted.key);
        let l = extracted.path.length;
        // No need to merge or build object
        if(l == 0){
            if(current instanceof Object && value instanceof Object){
                return Object.assign(current, value);
            }

            return value;
        }
        // Build or merge
        else{
            let base = extracted.path.reduceRight(
                (previousItem, currentItem, index) => {
                    return {[currentItem]: (l - 1 == index ?value :previousItem)};
                },
                {}
            );

            if(current instanceof Object){
                return Merge(current, base);
            }

            return base;
        }
    }

    [$fnResolveGet](extracted, value, def){
        return ((p, o) => {
            let isDefault = false;
            let value = p.reduce((xs, x) => (xs && xs[x]) ?xs[x] :((isDefault = true) && null), o);

            return !isDefault ?value :def;
        })(extracted.path, value);
    }

    /**
     * Set Options
     *
     * @param {Object} options
     */
    setOptions(options = {}) {
        this.options = Object.assign(this.options, options);
    }

    /**
     * Set item
     *
     * @param {string} name
     * @param {*} value
     * @param {number} expire - seconds
     *
     * @todo: issue, expire in objects works only for root value, nested objects not supported yet
     */
    set(name, value, expire = null) {
        let extracted = this[$fnExtractKey](name);
        value = this[$fnResolveSet](extracted, value);

        // @note undefined not valid type for json, convert it into null
        if(typeof value == 'undefined'){
            value = null;
        }

        const stringifyValue = JSON.stringify({
            value: value,
            expire: expire !== null ? new Date().getTime() + expire : null,
        });

        this.storage.setItem(this.options.namespace + extracted.key, stringifyValue);
    }

    /**
     * Get item
     *
     * @param {string} name
     * @param {*} def - default value
     * @returns {*}
     *
     * @todo: issue, expire in objects works only for root value, nested objects not supported yet
     */
    get(name, def = null) {
        let extracted = this[$fnExtractKey](name);
        const item = this.storage.getItem(this.options.namespace + extracted.key);

        if (item !== null) {
            try {
                const data = JSON.parse(item);

                if(
                    data.expire === null
                    || data.expire >= new Date().getTime()
                ){
                    return extracted.path.length == 0
                        ?data.value
                        :this[$fnResolveGet](extracted, data.value, def)
                        ;
                }

                this.remove(extracted.key);
            }
            catch (err) {
                return def;
            }
        }

        return def;
    }

    /**
     * Get item by key
     *
     * @param {number} index
     * @return {*}
     */
    key(index) {
        return this.storage.key(index);
    }

    /**
     * Remove item
     *
     * @param {string} name
     * @return {boolean}
     */
    remove(name) {
        return this.storage.removeItem(this.options.namespace + name);
    }

    /**
     * Clear storage
     */
    clear() {
        if (this.length === 0) {
            return;
        }

        const removedKeys = [];

        for (let i = 0; i < this.length; i++) {
            const key = this.storage.key(i);
            const regexp = new RegExp(`^${this.options.namespace}.+`, 'i');

            if (regexp.test(key) === false) {
                continue;
            }

            removedKeys.push(key);
        }

        for (const key in removedKeys) {
            this.storage.removeItem(removedKeys[key]);
        }
    }

    /**
     * Add storage change event
     *
     * @param {string} name
     * @param {Function} callback
     */
    on(name, callback) {
        StorageEvent.on(this.options.namespace + name, callback);
    }

    /**
     * Remove storage change event
     *
     * @param {string} name
     * @param {Function} callback
     */
    off(name, callback) {
        StorageEvent.off(this.options.namespace + name, callback);
    }
}
