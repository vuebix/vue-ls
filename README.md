# vue-ls

Vue plugin for work with LocalStorage from Vue context. Simplified fork of [https://github.com/RobinCK/vue-ls](https://github.com/RobinCK/vue-ls)

## Install

#### NPM

``` bash
npm install bitbucket:vuebix/vue-ls --save
```

## Usage

Vue localStorage API.

``` js
import VueLocalStorage from 'vue-ls';

options = {
  namespace: 'vuejs__'
};

Vue.use(VueLocalStorage, options);

//or
//Vue.use(VueLocalStorage);

new Vue({
    el: '#app',
    mounted: function() {
        Vue.ls.set('foo', 'boo');
        //Set expire for item
        Vue.ls.set('foo', 'boo', 60 * 60 * 1000); //expiry 1 hour
        Vue.ls.get('foo');
        Vue.ls.get('boo', 10); //if not set boo returned default 10
        
        let callback = (val, oldVal, uri) => {
          console.log('localStorage change', val);
        } 
        
        Vue.ls.on('foo', callback) //watch change foo key and triggered callback
        Vue.ls.off('foo', callback) //unwatch
        
        Vue.ls.remove('foo');
    }
});
```

#### Global

- `Vue.ls`
 
#### Context
- `this.$ls`

## API

#### `Vue.ls.get(name, def)`

Returns value under `name` in local storage. Internally parses the value from JSON before returning it.

- `def`: default null, returned if not set `name`.

#### `Vue.ls.set(name, value, expire)`

Persists `value` under `name` in local storage. Internally converts the `value` to JSON.

- `expire`: default null, life time in milliseconds `name`

#### `Vue.ls.remove(name)`

Removes `name` from local storage. Returns `true` if the property was successfully deleted, and `false` otherwise.

#### `Vue.ls.clear()`

Clears local storage.

#### `Vue.ls.on(name, callback)`

Listen for changes persisted against `name` on other tabs. Triggers `callback` when a change occurs, passing the following arguments.

- `newValue`: the current value for `name` in local storage, parsed from the persisted JSON
- `oldValue`: the old value for `name` in local storage, parsed from the persisted JSON
- `url`: the url for the tab where the modification came from

#### `Vue.ls.off(name, callback)`

Removes a listener previously attached with `Vue.ls.on(name, callback)`.

## Note
Some browsers don't support the storage event, and most of the browsers that do support it will only call it when the storage is changed by a different window. So, open your page up in two windows. Click the links in one window and you will probably see the event in the other.

The assumption is that your page will already know all interactions with localStorage in its own window and only needs notification when a different window changes things. This, of course, is a foolish assumption. But.

## License
MIT

This project is based on the great work of `vue-ls` written by [Igor Ognichenko](https://github.com/RobinCK). [RobinCK/vue-ls](https://github.com/RobinCK/vue-ls)
